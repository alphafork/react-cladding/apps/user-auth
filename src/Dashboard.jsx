import React, { useRef } from 'react';
import { OverlayPanel } from 'primereact/overlaypanel';
import { Menu } from 'primereact/menu';
import DashboardContainer, {
  TopBar, SideBar, Container, Body, Footer,
} from '@ims/dashboard';

function Dashboard() {
  const menu = useRef(null);
  const menuItems = [
    { label: 'Logout', icon: 'pi pi-fw pi-sign-out', url: '/logout' },
  ];

  return (
    <DashboardContainer>
      <TopBar>
        <div className="layout-topbar-menu">
          <button type="button" className="p-link layout-topbar-button" onClick={(e) => menu.current.toggle(e)}>
            <i className="pi pi-user" />
            <span>Profile</span>
          </button>
          <OverlayPanel ref={menu}>
            <Menu model={menuItems} />
          </OverlayPanel>
        </div>
      </TopBar>
      <SideBar />
      <Container>
        <Body />
        <Footer />
      </Container>
    </DashboardContainer>
  );
}

export default Dashboard;
