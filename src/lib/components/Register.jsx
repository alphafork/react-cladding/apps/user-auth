import React, { useEffect, useRef, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { Button } from 'primereact/button';
import { InputText } from 'primereact/inputtext';
import { Password } from 'primereact/password';
import { Toast } from 'primereact/toast';
import http from '../utils/httpClient';

function Register() {
  const [email, setEmail] = useState('');
  const [password1, setPassword1] = useState('');
  const [password2, setPassword2] = useState('');
  const [isSubmitted, setIsSubmitted] = useState(false);
  const navigate = useNavigate();
  const toast = useRef(null);

  const showAlert = ({ type, msg }) => {
    toast.current.show({
      severity: type,
      summary: type.toUpperCase(),
      detail: msg,
    });
  };

  const onSubmit = (e) => {
    e.preventDefault();
    setIsSubmitted(true);
  };

  useEffect(() => {
    if (isSubmitted) {
      const register = async () => {
        const data = { email, password1, password2 };
        try {
          const response = await http.post('/auth/registration/', data);
          if (response.status === 204) {
            navigate('/login');
          }
        } catch (error) {
          const { errors } = error.response.data;
          for (const err of errors) {
            const { attr, detail } = err;
            showAlert({
              type: 'error',
              msg: `${attr}: ${detail}`,
            });
          }
        }
      };
      register();
    }
    setIsSubmitted(false);
  }, [isSubmitted, email, password1, password2, navigate]);

  return (
    <div className="flex mt-8 align-items-center justify-content-center">
      <div className="surface-card p-4 shadow-2 border-round w-full lg:w-4">
        <div className="text-center m-3">
          <div className="text-900 text-3xl font-medium mb-3">Sign up for a new account</div>
          <span className="text-600 font-medium line-height-3">Already have an account?</span>
          <a href="/login" className="font-medium no-underline ml-2 text-blue-500 cursor-pointer">Sign in</a>
        </div>
        <form onSubmit={onSubmit}>
          <div>
            <label htmlFor="email" className="block text-900 font-medium mb-2">
              Email
            </label>
            <InputText
              className="w-full mb-3"
              id="email"
              type="text"
              placeholder="Email address"
              name="email"
              value={email}
              onChange={(e) => { setEmail(e.target.value); }}
            />

            <label
              htmlFor="password"
              className="block text-900 font-medium mb-2"
            >
              Password
            </label>
            <Password
              className="w-full"
              inputClassName="w-full p-3"
              inputid="password1"
              type="password"
              placeholder="Password"
              name="password1"
              value={password1}
              toggleMask
              onChange={(e) => { setPassword1(e.target.value); }}
            />

            <label
              htmlFor="password2"
              className="block text-900 font-medium mt-3 mb-2"
            >
              Confirm Password
            </label>
            <InputText
              className="w-full mb-3"
              id="password2"
              type="password"
              placeholder="Password"
              name="password2"
              value={password2}
              onChange={(e) => { setPassword2(e.target.value); }}
            />

            <Button label="Sign up" aria-label="Sign up" icon="pi pi-user" className="w-full" />
          </div>
        </form>
      </div>
      <Toast ref={toast} position="bottom-center" />
    </div>
  );
}

export default Register;
