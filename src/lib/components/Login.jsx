import React, { useEffect, useRef, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { Button } from 'primereact/button';
import { InputText } from 'primereact/inputtext';
import { Toast } from 'primereact/toast';
import { ProgressSpinner } from 'primereact/progressspinner';
import http from '../utils/httpClient';
import '@ims/dashboard/dist/style.css';
import imgUrl from '../assets/images/register-1.png';

function Login({ redirectTo }) {
  const navigate = useNavigate();
  const [isLoggedIn, setIsLoggedIn] = useState(null);
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const toast = useRef(null);

  const showAlert = ({ type, msg }) => {
    toast.current.show({
      severity: type,
      summary: type.toUpperCase(),
      detail: msg,
    });
  };

  useEffect(() => {
    if (isLoggedIn) {
      !!redirectTo && navigate(redirectTo);
    }
  }, [isLoggedIn, redirectTo, navigate]);

  useEffect(() => {
    (async () => {
      const key = window.localStorage.getItem('AUTH_TOKEN');
      try {
        const response = await http.get(
          '/auth/user/',
          {
            headers: {
              Authorization: `Token ${key}`,
            },
          },
        );
        if (response?.status === 200) {
          setIsLoggedIn(true);
        }
      } catch (error) {
        setIsLoggedIn(false);
      }
    })();
  }, []);

  const onSubmit = async (e) => {
    e.preventDefault();
    try {
      const response = await http.post('/auth/login/', { email, password });
      if (response?.data?.key != null) {
        const { key } = response.data;
        window.localStorage.setItem('AUTH_TOKEN', key);
        setIsLoggedIn(true);
      }
    } catch (error) {
      const { errors } = error.response.data;
      for (const err of errors) {
        const { attr, detail } = err;
        showAlert({
          type: 'error',
          msg: `${attr}: ${detail}`,
        });
      }
      setIsLoggedIn(false);
    }
  };

  if (setIsLoggedIn === null) {
    return (
      <div className="flex justify-content-center m-5">
        <ProgressSpinner />
      </div>
    );
  }

  return (
    <>
      <div className="grid grid-nogutter surface-0 text-800">
        <div className="col-12 md:col-6 px-8 text-center md:text-left flex align-items-center ">
          <section className="ml-5">
            <div className="text-6xl text-primary font-bold mb-3">Sign in to you account</div>
            <p className="mt-0 mb-4 pl-3 text-700 line-height-3">Don&apos;t have an account? 

              <a href="/register" className="font-medium no-underline ml-2 text-blue-500 cursor-pointer">Sign up</a>
            </p>
            <div className="text-start m-3">
              <div className="text-600 font-medium line-height-3">To enroll a new student go to
                <a href="/student-registration" className="font-medium no-underline ml-2 text-blue-500 cursor-pointer">Student registration</a>
              </div>
            </div>

            <form onSubmit={onSubmit}>
              <div>
                <label htmlFor="email" className="block text-900 font-medium mb-2">
                  Email
                </label>
                <InputText
                  className="w-full mb-3"
                  id="email"
                  type="text"
                  placeholder="Email address"
                  name="email"
                  value={email}
                  onChange={(e) => { setEmail(e.target.value); }}
                />

                <label
                  htmlFor="password"
                  className="block text-900 font-medium mb-2"
                >
                  Password
                </label>
                <InputText
                  className="w-full mb-3"
                  id="password"
                  type="password"
                  placeholder="Password"
                  name="password"
                  value={password}
                  onChange={(e) => { setPassword(e.target.value); }}
                />

                <Button label="Sign In" aria-label="Sign In" icon="pi pi-user" className="mr-3 p-button-raised" />
              </div>
            </form>

          </section>
        </div>
        <div className="col-12 md:col-6 overflow-hidden">
          <img src={imgUrl} alt="hero-1" className="md:ml-auto block md:h-full" style={{ clipPath: 'polygon(8% 0, 100% 0%, 100% 100%, 0 100%)' }} />
        </div>

        <Toast ref={toast} position="bottom-center" />
      </div>
    </>
  );
}

export default Login;
