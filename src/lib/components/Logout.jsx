import { useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import http from '../utils/httpClient';

function Logout({ redirectTo = '/' }) {
  const navigate = useNavigate();

  useEffect(() => {
    (async () => {
      const response = await http.post('/auth/logout/');
      if (response?.data?.detail != null) {
        window.localStorage.removeItem('AUTH_TOKEN');
        navigate(redirectTo);
      }
    })();
  }, [navigate, redirectTo]);
}

export default Logout;
