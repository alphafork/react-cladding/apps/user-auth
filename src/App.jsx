import React from 'react';
import { Routes, Route } from 'react-router-dom';
import Dashboard from './Dashboard';
import { Login, Logout, Register } from './lib/main';
import '@ims/dashboard/dist/style.css';

function App() {
  return (
    <Routes>
      <Route index element={<Login redirectTo="/dashboard" />} />
      <Route path="login" element={<Login redirectTo="/dashboard" />} />
      <Route path="dashboard" element={<Dashboard />} />
      <Route path="logout" element={<Logout />} />
      <Route path="register" element={<Register />} />
    </Routes>
  );
}

export default App;
