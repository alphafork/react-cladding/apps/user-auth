import { resolve } from 'path';
import { defineConfig, loadEnv } from 'vite';
import react from '@vitejs/plugin-react';
import { peerDependencies } from './package.json';

// https://vitejs.dev/config/
export default defineConfig(({ command, mode }) => {
  // Load environment variables from both .env file and from terminal.
  // Variables set from terminal will have priority over the ones from file.
  const envFromFile = loadEnv(mode, process.cwd(), '');
  const env = {
    ...envFromFile,
    ...process.env,
  }

  const config = {
    build: {
      lib: {
        entry: resolve(__dirname, 'src/lib/main.jsx'),
        formats: ['es'],
        fileName: '@ims/user-auth',
      },
      rollupOptions: {
        external: Object.keys(peerDependencies),
      },
    },
    plugins: [react()],
  };

  // Replace `process.env` variables only in development mode or when building
  // as app.
  if (mode === 'development' || env.BUILD_AS_APP === 'true') {
    config.define = {
      'process.env': env,
    }
  }

  return config;
});
